// NOTE: Options are commented out deliberately, since Prettier's defaults are sensible and should be used whenever
// possible. File is still here should there ever be the need to change options.
module.exports = {
//   semi: true,
//   trailingComma: "all",
//   singleQuote: true,
//   printWidth: 80,
//   tabWidth: 2,
};
